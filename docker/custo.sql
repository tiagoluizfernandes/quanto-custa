--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.2
-- Dumped by pg_dump version 9.3.2
-- Started on 2014-02-06 23:46:52

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 183 (class 3079 OID 11750)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2007 (class 0 OID 0)
-- Dependencies: 183
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 177 (class 1259 OID 16438)
-- Name: fornecedor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE fornecedor (
    codigo_fornecedor integer NOT NULL,
    nome_fornecedor character varying(60) NOT NULL
);


ALTER TABLE public.fornecedor OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 16436)
-- Name: fornecedor_codigo_fornecedor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE fornecedor_codigo_fornecedor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fornecedor_codigo_fornecedor_seq OWNER TO postgres;

--
-- TOC entry 2008 (class 0 OID 0)
-- Dependencies: 176
-- Name: fornecedor_codigo_fornecedor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE fornecedor_codigo_fornecedor_seq OWNED BY fornecedor.codigo_fornecedor;


--
-- TOC entry 175 (class 1259 OID 16430)
-- Name: item; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE item (
    codigo_item integer NOT NULL,
    descricao_item character varying(60) NOT NULL,
    descricao_abreviada_item character varying(10) NOT NULL
);


ALTER TABLE public.item OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 16428)
-- Name: item_codigo_item_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE item_codigo_item_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.item_codigo_item_seq OWNER TO postgres;

--
-- TOC entry 2009 (class 0 OID 0)
-- Dependencies: 174
-- Name: item_codigo_item_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE item_codigo_item_seq OWNED BY item.codigo_item;


--
-- TOC entry 178 (class 1259 OID 16444)
-- Name: item_fornecedor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE item_fornecedor (
    codigo_fornecedor integer NOT NULL,
    codigo_item integer NOT NULL,
    cod_unidade_medida_forn integer,
    qtd_fornecida numeric(14,7) NOT NULL,
    valor_qtd_fornecida numeric(15,2) NOT NULL
);


ALTER TABLE public.item_fornecedor OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 16462)
-- Name: item_kit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE item_kit (
    codigo_kit integer NOT NULL,
    codigo_item integer NOT NULL,
    cod_unidade_medida_kit integer NOT NULL,
    qtd_item_kit numeric(14,7) NOT NULL
);


ALTER TABLE public.item_kit OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 16456)
-- Name: kit; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE kit (
    codigo_kit integer NOT NULL,
    descricao_kit character varying(60) NOT NULL,
    descricao_abreviada_kit character varying(10) NOT NULL
);


ALTER TABLE public.kit OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 16454)
-- Name: kit_codigo_kit_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE kit_codigo_kit_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.kit_codigo_kit_seq OWNER TO postgres;

--
-- TOC entry 2010 (class 0 OID 0)
-- Dependencies: 180
-- Name: kit_codigo_kit_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE kit_codigo_kit_seq OWNED BY kit.codigo_kit;


--
-- TOC entry 173 (class 1259 OID 16422)
-- Name: unidade_medida; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE unidade_medida (
    cod_unidade_medida integer NOT NULL,
    descricao_unidade_medida character varying(60) NOT NULL,
    sigla_unidade_medida character varying(10) NOT NULL
);


ALTER TABLE public.unidade_medida OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 16420)
-- Name: unidade_medida_cod_unidade_medida_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE unidade_medida_cod_unidade_medida_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.unidade_medida_cod_unidade_medida_seq OWNER TO postgres;

--
-- TOC entry 2011 (class 0 OID 0)
-- Dependencies: 172
-- Name: unidade_medida_cod_unidade_medida_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE unidade_medida_cod_unidade_medida_seq OWNED BY unidade_medida.cod_unidade_medida;


--
-- TOC entry 179 (class 1259 OID 16449)
-- Name: unidade_medida_conversao; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE unidade_medida_conversao (
    cod_unidade_medida_de integer NOT NULL,
    cod_unidade_medida_para integer NOT NULL,
    fator_conversao numeric(14,7) NOT NULL
);


ALTER TABLE public.unidade_medida_conversao OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 16404)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    codigo_usuario integer NOT NULL,
    login character varying(15) NOT NULL,
    nome character varying(100),
    senha character varying(100),
    data_alteracao date,
    ativo boolean
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 16402)
-- Name: usuario_codigo_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuario_codigo_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_codigo_usuario_seq OWNER TO postgres;

--
-- TOC entry 2012 (class 0 OID 0)
-- Dependencies: 170
-- Name: usuario_codigo_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuario_codigo_usuario_seq OWNED BY usuario.codigo_usuario;


--
-- TOC entry 1862 (class 2604 OID 24607)
-- Name: codigo_fornecedor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY fornecedor ALTER COLUMN codigo_fornecedor SET DEFAULT nextval('fornecedor_codigo_fornecedor_seq'::regclass);


--
-- TOC entry 1861 (class 2604 OID 24608)
-- Name: codigo_item; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY item ALTER COLUMN codigo_item SET DEFAULT nextval('item_codigo_item_seq'::regclass);


--
-- TOC entry 1863 (class 2604 OID 24609)
-- Name: codigo_kit; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY kit ALTER COLUMN codigo_kit SET DEFAULT nextval('kit_codigo_kit_seq'::regclass);


--
-- TOC entry 1860 (class 2604 OID 24610)
-- Name: cod_unidade_medida; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY unidade_medida ALTER COLUMN cod_unidade_medida SET DEFAULT nextval('unidade_medida_cod_unidade_medida_seq'::regclass);


--
-- TOC entry 1859 (class 2604 OID 24611)
-- Name: codigo_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario ALTER COLUMN codigo_usuario SET DEFAULT nextval('usuario_codigo_usuario_seq'::regclass);


--
-- TOC entry 1994 (class 0 OID 16438)
-- Dependencies: 177
-- Data for Name: fornecedor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY fornecedor (codigo_fornecedor, nome_fornecedor) FROM stdin;
\.


--
-- TOC entry 2013 (class 0 OID 0)
-- Dependencies: 176
-- Name: fornecedor_codigo_fornecedor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('fornecedor_codigo_fornecedor_seq', 1, false);


--
-- TOC entry 1992 (class 0 OID 16430)
-- Dependencies: 175
-- Data for Name: item; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY item (codigo_item, descricao_item, descricao_abreviada_item) FROM stdin;
1	teste test	teste 2
2	 teste item teste jjjjjjjjjjjjjjjjjjjjjddddddddddddddddddddd	outro ite2
\.


--
-- TOC entry 2014 (class 0 OID 0)
-- Dependencies: 174
-- Name: item_codigo_item_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('item_codigo_item_seq', 2, true);


--
-- TOC entry 1995 (class 0 OID 16444)
-- Dependencies: 178
-- Data for Name: item_fornecedor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY item_fornecedor (codigo_fornecedor, codigo_item, cod_unidade_medida_forn, qtd_fornecida, valor_qtd_fornecida) FROM stdin;
\.


--
-- TOC entry 1999 (class 0 OID 16462)
-- Dependencies: 182
-- Data for Name: item_kit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY item_kit (codigo_kit, codigo_item, cod_unidade_medida_kit, qtd_item_kit) FROM stdin;
\.


--
-- TOC entry 1998 (class 0 OID 16456)
-- Dependencies: 181
-- Data for Name: kit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY kit (codigo_kit, descricao_kit, descricao_abreviada_kit) FROM stdin;
\.


--
-- TOC entry 2015 (class 0 OID 0)
-- Dependencies: 180
-- Name: kit_codigo_kit_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('kit_codigo_kit_seq', 1, false);


--
-- TOC entry 1990 (class 0 OID 16422)
-- Dependencies: 173
-- Data for Name: unidade_medida; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY unidade_medida (cod_unidade_medida, descricao_unidade_medida, sigla_unidade_medida) FROM stdin;
\.


--
-- TOC entry 2016 (class 0 OID 0)
-- Dependencies: 172
-- Name: unidade_medida_cod_unidade_medida_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('unidade_medida_cod_unidade_medida_seq', 1, false);


--
-- TOC entry 1996 (class 0 OID 16449)
-- Dependencies: 179
-- Data for Name: unidade_medida_conversao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY unidade_medida_conversao (cod_unidade_medida_de, cod_unidade_medida_para, fator_conversao) FROM stdin;
\.


--
-- TOC entry 1988 (class 0 OID 16404)
-- Dependencies: 171
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY usuario (codigo_usuario, login, nome, senha, data_alteracao, ativo) FROM stdin;
2	william	William Tomazoni	UKzjVytTht6igKtEgYIJTA==	2014-02-02	t
3	tiago	tiago luiz fernades	xqKRM97vaPofc63kzdlFfA==	2014-02-02	t
\.


--
-- TOC entry 2017 (class 0 OID 0)
-- Dependencies: 170
-- Name: usuario_codigo_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('usuario_codigo_usuario_seq', 3, true);


--
-- TOC entry 1871 (class 2606 OID 16443)
-- Name: pk_fornecedor; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY fornecedor
    ADD CONSTRAINT pk_fornecedor PRIMARY KEY (codigo_fornecedor);


--
-- TOC entry 1869 (class 2606 OID 16435)
-- Name: pk_item; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY item
    ADD CONSTRAINT pk_item PRIMARY KEY (codigo_item);


--
-- TOC entry 1873 (class 2606 OID 16448)
-- Name: pk_item_fornecedor; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY item_fornecedor
    ADD CONSTRAINT pk_item_fornecedor PRIMARY KEY (codigo_fornecedor, codigo_item);


--
-- TOC entry 1879 (class 2606 OID 16466)
-- Name: pk_item_kit; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY item_kit
    ADD CONSTRAINT pk_item_kit PRIMARY KEY (codigo_kit, codigo_item);


--
-- TOC entry 1877 (class 2606 OID 16461)
-- Name: pk_kit; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY kit
    ADD CONSTRAINT pk_kit PRIMARY KEY (codigo_kit);


--
-- TOC entry 1867 (class 2606 OID 16427)
-- Name: pk_unidade_medida; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY unidade_medida
    ADD CONSTRAINT pk_unidade_medida PRIMARY KEY (cod_unidade_medida);


--
-- TOC entry 1875 (class 2606 OID 16453)
-- Name: pk_unidade_medida_conversao; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY unidade_medida_conversao
    ADD CONSTRAINT pk_unidade_medida_conversao PRIMARY KEY (cod_unidade_medida_de, cod_unidade_medida_para);


--
-- TOC entry 1865 (class 2606 OID 16409)
-- Name: pk_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT pk_usuario PRIMARY KEY (codigo_usuario);


--
-- TOC entry 2006 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2014-02-06 23:46:52

--
-- PostgreSQL database dump complete
--

