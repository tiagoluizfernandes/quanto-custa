package dao.implementation;

import java.util.List;
import model.Usuario;

public interface IUsuarioDAO {

	public Usuario consultarUsuario(Usuario usuario) throws Exception;
	
	public int inserirUsuario(Usuario usuario) throws Exception;

        public int alterarUsuario(Usuario usuario) throws Exception;

        public List<Usuario> pesquisarUsuario(Usuario usuario) throws Exception;
}
