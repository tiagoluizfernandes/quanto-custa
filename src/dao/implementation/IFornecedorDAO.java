package dao.implementation;

import java.util.List;
import model.Fornecedor;
import util.ItemComboBox;

public interface IFornecedorDAO {

	public List<Fornecedor> consultarFornecedor(Fornecedor fornecedor, String ambos) throws Exception ;
	
	public int inserirFornecedor(Fornecedor fornecedor) throws Exception;

        public int alterarFornecedor(Fornecedor fornecedor) throws Exception;
        
        public List<ItemComboBox> buscarFornecedores() throws Exception;
    
}
