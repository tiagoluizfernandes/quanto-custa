package dao.implementation;

import java.util.List;
import model.Item;
import util.ItemComboBox;

public interface IItemDAO {

	public List<Item> consultarItem(Item produto) throws Exception;
	
	public int inserirItem(Item produto) throws Exception;

        public int alterarItem(Item produto) throws Exception;

}
