package dao.realization;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.implementation.IUsuarioDAO;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Usuario;
import util.Util;

public class UsuarioDAO implements IUsuarioDAO {

    public Usuario consultarUsuario(Usuario usuario) throws Exception {
        Usuario user = new Usuario();
        try {
            PreparedStatement stmt = Util.conexao().prepareStatement("select nome,senha,login,data_alteracao,ativo from usuario where login=? and ativo=true");
            stmt.setString(1, usuario.getLogin());
            ResultSet res = stmt.executeQuery();
            while (res.next()) {
                if (usuario.getSenha().equals(Util.descriptografa(res.getString("senha")))) {
                    user.setNome(res.getString("nome"));
                    user.setLogin(res.getString("login"));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    user.setData_alteracao(sdf.parse(res.getString("data_alteracao")));
                    user.setAtivo(res.getBoolean("ativo"));
                    return user;
                }
            }
            return null;
        } catch (SQLException e) {
            System.out.println("consultarUsuario(): " + e.toString());
        }
        return null;
    }

    public List<Usuario> pesquisarUsuario(Usuario usuario) throws Exception {
        List<Usuario> user = new ArrayList<Usuario>();
        try {
            PreparedStatement stmt = Util.conexao().prepareStatement("select * from usuario where upper(nome) like upper('%" + usuario.getNome() + "%')");
            ResultSet res = stmt.executeQuery();
            while (res.next()) {
                Usuario u = new Usuario();
                u.setNome(res.getString("nome"));
                u.setLogin(res.getString("login"));
                u.setSenha(res.getString("senha"));
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                u.setData_alteracao(sdf.parse(res.getString("data_alteracao")));
                u.setAtivo(res.getBoolean("ativo"));
                user.add(u);
            }
            return user;
        } catch (SQLException e) {
            System.out.println("pesquisarUsuario(): " + e.toString());
        }
        return null;
    }

    public int inserirUsuario(Usuario usuario) throws Exception {
        try {
            PreparedStatement verif = Util.conexao().prepareStatement("select * from usuario where login = ?");
            verif.setString(1, usuario.getLogin());
            ResultSet res = verif.executeQuery();
            if (res.next()) {
                return 1;
            }
            PreparedStatement stmt = Util.conexao().prepareStatement("INSERT INTO usuario("+
            "login, nome, senha, data_alteracao, ativo)"+
            "VALUES (?, ?, ?, current_timestamp, ?)");
            int i = 1;
            stmt.setString(i++, usuario.getLogin());            
            stmt.setString(i++, usuario.getNome());
            stmt.setString(i++, usuario.getSenha());
            stmt.setBoolean(i++, true);
            stmt.executeUpdate();
            return 0;
        } catch (SQLException e) {
            System.out.println("inserirUsuario(): " + e.toString());
            return -1;
        }
    }

    public int alterarUsuario(Usuario usuario) throws Exception {
        try {
            PreparedStatement stmt = Util.conexao().prepareStatement("update usuario set nome=?,senha=?,data_alteracao=current_timestamp,ativo=? where login=?");
            int i = 1;
            stmt.setString(i++, usuario.getNome());
            stmt.setString(i++, usuario.getSenha());
            stmt.setBoolean(i++, usuario.isAtivo());
            stmt.setString(i++, usuario.getLogin());
            stmt.executeUpdate();
            return 0;
        } catch (SQLException e) {
            System.out.println("alterarUsuario(): " + e.toString());
            return -1;
        }
    }
}
