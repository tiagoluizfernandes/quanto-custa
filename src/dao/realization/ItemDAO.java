package dao.realization;

import dao.implementation.IItemDAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Item;
import util.ItemComboBox;
import util.Util;

public class ItemDAO implements IItemDAO {
    
    @Override
    public List<Item> consultarItem(Item item) throws Exception {
        List<Item> it = new ArrayList<Item>();
        try {
            StringBuffer query = new StringBuffer();
            query.append("select codigo_item, descricao_item, descricao_abreviada_item ");
            query.append("from item where (upper(descricao_abreviada_item) like upper('%");
            query.append(item.getDescricao_abreviada_item());
            query.append("%') or '' = '"+item.getDescricao_abreviada_item()+"')");
            System.out.println(query.toString());
            PreparedStatement stmt = Util.conexao().prepareStatement(query.toString());
            ResultSet res = stmt.executeQuery();
            System.out.println(stmt.toString());
            while (res.next()) {
                Item i = new Item();

                i.setCodigo_item(Integer.parseInt(res.getString("codigo_item")));
                i.setDescricao_abreviada_item(res.getString("descricao_abreviada_item"));
                i.setDescricao_item(res.getString("descricao_item"));
                System.out.println(stmt.toString());
                it.add(i);
            }
            return it;
        } catch (SQLException e) {
            System.out.println("consultarItem(): " + e.toString());
        }
        return null;
    }

    @Override
    public int inserirItem(Item item) throws Exception {
        PreparedStatement stmt = null;
        try {
            StringBuffer query = new StringBuffer();

            query.append("INSERT INTO item("
                    + "descricao_abreviada_item, descricao_item)"
                    + "VALUES (?, ?)");
            stmt = Util.conexao().prepareStatement(query.toString());
            int i = 1;

            stmt.setString(i++, item.getDescricao_abreviada_item());
            stmt.setString(i++, item.getDescricao_item());
     
            System.out.println(stmt.toString());
            stmt.executeUpdate();
            return 0;
        } catch (SQLException e) {
            System.out.println("inserirItem(): " + e.toString());
            throw new Exception();
        }finally{
            stmt.close();
        }
    }

    @Override
    public int alterarItem(Item item) throws Exception {
        try {
            StringBuffer query = new StringBuffer();
            PreparedStatement stmt;

            int i = 1;

            query.append(" UPDATE item ");
            query.append(" SET descricao_abreviada_item=?, descricao_item=? ");

            query.append("WHERE codigo_item=?");
            stmt = Util.conexao().prepareStatement(query.toString());

            stmt.setString(i++, item.getDescricao_abreviada_item());
            stmt.setString(i++, item.getDescricao_item());
            stmt.setInt(i++, item.getCodigo_item());
            System.out.println(stmt.toString());
            stmt.executeUpdate();
            return 0;
        } catch (SQLException e) {
            System.out.println("alterarItem(): " + e.toString());
            throw new Exception();
        }
    }
}
