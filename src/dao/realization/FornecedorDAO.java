package dao.realization;

import dao.implementation.IFornecedorDAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import model.Fornecedor;
import util.ItemComboBox;
import util.Util;

public class FornecedorDAO implements IFornecedorDAO {
    
    public List<Fornecedor> consultarFornecedor(Fornecedor fornecedor, String ambos) throws Exception {
        List<Fornecedor> forn = new ArrayList<Fornecedor>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            StringBuffer query = new StringBuffer();
            query.append("select * from fornecedor where (upper(razao) like upper('%");
            query.append(fornecedor.getNome());
            query.append("%') or '' = '"+fornecedor.getNome()+"')");
            query.append(" and (cnpj = '"+fornecedor.getCnpj()+"'");
            query.append(" or '' = '"+fornecedor.getCnpj()+"')");
            if (ambos.equals("Não")) {
                if (fornecedor.isAtivo()) {
                    query.append(" and ativo = true");
                } else {
                    query.append(" and ativo = false");
                }
            }
            System.out.println(query.toString());
            PreparedStatement stmt = Util.conexao().prepareStatement(query.toString());
            ResultSet res = stmt.executeQuery();
            System.out.println(stmt.toString());
            while (res.next()) {
                Fornecedor c = new Fornecedor();

                c.setCodigo_fornecedor(Integer.parseInt(res.getString("codigo_fornecedor")));
                c.setRazao(res.getString("razao"));
                c.setNome(res.getString("nome"));
                c.setCnpj(res.getString("cnpj"));
                c.setRamo(res.getString("ramo"));
                c.setEndereco(res.getString("endereco"));
                c.setNumero(res.getString("numero"));
                c.setComplemento(res.getString("complemento"));
                c.setCep(res.getString("cep"));
                c.setCidade(res.getString("cidade"));
                c.setEstado(res.getString("estado"));
                c.setTelefone(res.getString("telefone"));
                c.setContato(res.getString("contato"));
                c.setData_alteracao(sdf.parse(res.getString("data_alteracao")));
                c.setAtivo(res.getBoolean("ativo"));
                System.out.println(stmt.toString());
                forn.add(c);
            }
            return forn;
        } catch (SQLException e) {
            System.out.println("consultarFornecedor(): " + e.toString());
        }
        return null;
    }

    @Override
    public int inserirFornecedor(Fornecedor fornecedor) throws Exception {
        PreparedStatement stmt = null;
        try {
            StringBuffer query = new StringBuffer();

            query.append("INSERT INTO fornecedor("
                    + "razao, nome, cnpj, ramo, endereco, numero, complemento, cep, "
                    + "cidade, estado, telefone, contato, data_alteracao, ativo)"
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, "
                    + "?, ?, ?, current_timestamp, true)");
            stmt = Util.conexao().prepareStatement(query.toString());
            int i = 1;

            stmt.setString(i++, fornecedor.getRazao());
            stmt.setString(i++, fornecedor.getNome());
            stmt.setString(i++, fornecedor.getCnpj());
            stmt.setString(i++, fornecedor.getRamo());
            stmt.setString(i++, fornecedor.getEndereco());
            stmt.setString(i++, fornecedor.getNumero());
            stmt.setString(i++, fornecedor.getComplemento());
            stmt.setString(i++, fornecedor.getCep());
            stmt.setString(i++, fornecedor.getCidade());
            stmt.setString(i++, fornecedor.getEstado());
            stmt.setString(i++, fornecedor.getTelefone());
            stmt.setString(i++, fornecedor.getContato());
            
            //verifica se jah existe
            PreparedStatement verif = Util.conexao().prepareStatement("select * from fornecedor where cnpj = ?");
            verif.setString(1, fornecedor.getCnpj());
            ResultSet res = verif.executeQuery();
            if (res.next()) {
                return 1;
            }            
     
            System.out.println(stmt.toString());
            stmt.executeUpdate();
            return 0;
        } catch (SQLException e) {
            System.out.println("inserirFornecedor(): " + e.toString());
            throw new Exception();
        }finally{
            stmt.close();
        }
    }

    @Override
    public int alterarFornecedor(Fornecedor fornecedor) throws Exception {
        try {
            PreparedStatement verif = Util.conexao().prepareStatement("select * from fornecedor where cnpj = ? and codigo_fornecedor <> ?");
            verif.setString(1, fornecedor.getCnpj());
            verif.setInt(2, fornecedor.getCodigo_fornecedor());
            ResultSet res = verif.executeQuery();
            if (res.next()) {
                return 1;
            }            
            StringBuffer query = new StringBuffer();
            PreparedStatement stmt;// Util.conexao().prepareStatement(query.toString());

            int i = 1;

            query.append(" UPDATE fornecedor ");
            query.append(" SET razao=?, nome=?, cnpj=?, ramo=?, endereco=?, numero=?, complemento=?, "
                    + "cep=?, cidade=?, estado=?, telefone=?, contato=?, data_alteracao=current_timestamp, ativo=? ");

            query.append(" WHERE codigo_fornecedor=?");
            stmt = Util.conexao().prepareStatement(query.toString());

            stmt.setString(i++, fornecedor.getRazao());
            stmt.setString(i++, fornecedor.getNome());
            stmt.setString(i++, fornecedor.getCnpj());
            stmt.setString(i++, fornecedor.getRamo());
            stmt.setString(i++, fornecedor.getEndereco());
            stmt.setString(i++, fornecedor.getNumero());
            stmt.setString(i++, fornecedor.getComplemento());
            stmt.setString(i++, fornecedor.getCep());
            stmt.setString(i++, fornecedor.getCidade());
            stmt.setString(i++, fornecedor.getEstado());
            stmt.setString(i++, fornecedor.getTelefone());
            stmt.setString(i++, fornecedor.getContato());
            stmt.setBoolean(i++, fornecedor.isAtivo());

            stmt.setInt(i++, fornecedor.getCodigo_fornecedor());
            System.out.println(stmt.toString());
            stmt.executeUpdate();
            return 0;
        } catch (SQLException e) {
            System.out.println("alterarFornecedor(): " + e.toString());
            throw new Exception();
        }
    }
    
    @Override
    public List<ItemComboBox> buscarFornecedores() throws Exception {
        List<ItemComboBox> padrinhos = new ArrayList<ItemComboBox>();
        try {
            PreparedStatement stmt = Util.conexao().prepareStatement("select codigo_fornecedor, nome from fornecedor where ativo = true");
            ResultSet res = stmt.executeQuery();
            padrinhos.add(new ItemComboBox(0, ""));
            while (res.next()) {
                padrinhos.add(new ItemComboBox(res.getInt("codigo_fornecedor"), res.getString("nome")));
            }
            return padrinhos;
        } catch (SQLException e) {
            System.out.println("buscarFornecedores(): " + e.toString());
        }
        return null;
    }    
}
