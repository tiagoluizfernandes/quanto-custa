package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import model.Fornecedor;

public class FornecedorTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;
    private List<Fornecedor> fornecedores;

    public FornecedorTableModel() {
        fornecedores = new ArrayList<Fornecedor>();
    }

    public FornecedorTableModel(List<Fornecedor> lista) {
        this();
        fornecedores.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        // todas as colunas representam uma String
        return String.class;
    }

    @Override
    public int getColumnCount() {
        return 11;
    }

    @Override
    public String getColumnName(int coluna) {
        switch (coluna) {
            case 0:
                return "Razão Social";
            case 1:
                return "Nome";
            case 2:
                return "CNPJ";
            case 3:
                return "Ramo";
            case 4:
                return "Contato";                
            case 5:
                return "Endereço";
            case 6:
                return "Número";
            case 7:
                return "Cidade";
            case 8:
                return "Estado";
            case 9:
                return "Data Alteração";
            case 10:
                return "Ativo";
            default:
                return "";
        }
    }

    @Override
    public int getRowCount() {
        return fornecedores.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Fornecedor u = fornecedores.get(linha);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        switch (coluna) {
            case 0:
                return u.getRazao();
            case 1:
                return u.getNome();
            case 2:
                return u.getCnpj();
            case 3:
                return u.getRamo();
            case 4:
                return u.getContato();
            case 5:
                return u.getEndereco();
            case 6:
                return u.getNumero();
            case 7:
                return u.getCidade();
            case 8:
                return u.getEstado();
            case 9:
                return sdf.format(u.getData_alteracao());
            case 10:
                if (u.isAtivo()) {
                    return "Sim";
                } else {
                    return "Não";
                }
            default:
                return null;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna) {
        Fornecedor u = fornecedores.get(linha);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        switch (coluna) {
            case 0:
                u.setRazao(valor.toString());
                break;
            case 1:
                u.setNome(valor.toString());
                break;
            case 2:
                u.setCnpj(valor.toString());
                break;
            case 3:
                u.setRamo(valor.toString());
                break;
            case 4:
                u.setContato(valor.toString());
                break;
            case 5:
                u.setEndereco(valor.toString());
                break;
            case 6:
                u.setNumero(valor.toString());
                break;
            case 7:
                u.setCidade(valor.toString());
                break;
            case 8:
                u.setEstado(valor.toString());
                break;
            case 9:
                try {
                    u.setData_alteracao(sdf.parse(valor.toString()));
                } catch (ParseException ex) {
                    Logger.getLogger(FornecedorTableModel.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case 10:
                u.setAtivo(valor.toString().equals("true"));
                break;
        }

        fireTableDataChanged();
    }

    public Fornecedor getObjeto(int index) {
        return fornecedores.get(index);
    }

    public void adicionaLista(List<Fornecedor> lista) {
        int i = fornecedores.size() - 1;
        fornecedores.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    public void limpaLista() {
        int i = fornecedores.size();
        fornecedores.clear();
        if (i>0){
            fireTableRowsDeleted(0, i - 1);            
        }
    }
}
