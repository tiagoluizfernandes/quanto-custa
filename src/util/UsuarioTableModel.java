package util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import model.Usuario;

public class UsuarioTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;
    private List<Usuario> usuarios;

    public UsuarioTableModel() {
        usuarios = new ArrayList<Usuario>();
    }

    public UsuarioTableModel(List<Usuario> lista) {
        this();
        usuarios.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        // todas as colunas representam uma String
        return String.class;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int coluna) {
        switch (coluna) {
            case 0:
                return "Nome";
            case 1:
                return "Login";
            case 2:
                return "Data Alteração";
            case 3:
                return "Ativo";
            default:
                return "";
        }
    }

    @Override
    public int getRowCount() {
        return usuarios.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Usuario u = usuarios.get(linha);
        switch (coluna) {
            case 0:
                return u.getNome();
            case 1:
                return u.getLogin();
            case 2:
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                return sdf.format(u.getData_alteracao());
            case 3:
                if (u.isAtivo()) {
                    return "Sim";
                } else {
                    return "Não";
                }
            default:
                return null;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna) {
        Usuario u = usuarios.get(linha);
        switch (coluna) {
            case 0:
                u.setNome(valor.toString());
                break;
            case 1:
                u.setLogin(valor.toString());
                break;
            case 2:
                u.setData_alteracao(new Date(valor.toString()));
                break;
            case 3:
                u.setAtivo(valor.toString().equals("true"));
                break;                
        }
        
        fireTableDataChanged();
    }

    public Usuario getObjeto(int index) {
        return usuarios.get(index);
    }

    public void adicionaLista(List<Usuario> lista) {
        int i = usuarios.size()-1;
        usuarios.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    public void limpaLista() {
        int i = usuarios.size();
        usuarios.clear();
        if (i>0){
            fireTableRowsDeleted(0, i - 1);            
        }
    }
}
