package util;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class FormataRG extends PlainDocument {

    private int iMaxLength = 14;

    public boolean isNumber(String n) {
        boolean is;
        try {
            Long.parseLong(n);
            is = true;
        } catch (Exception e) {
            is = false;
        }
        return is;
    }

    @Override
    public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
        if (str == null) {
            return;
        }
        if (str.length() > 1) {
            String varrer = str;
            String param = "";
            for (int i = 0; i < varrer.length(); i++) {
                str = String.valueOf(varrer.charAt(i));
                if (Character.isLetter(str.charAt(0)) || ".".equals(str) || isNumber(str)
                        || "-".equals(str)) {
                    param = param+str;
                }
            }
            
            super.insertString(offset, param.toUpperCase(), attr);
        } else {
            if (Character.isLetter(str.charAt(0)) || ".".equals(str) || isNumber(str)
                    || "-".equals(str)) {

                int ilen = (getLength() + str.length());
                // se o comprimento final for menor aceita a str
                if (ilen <= iMaxLength) {
                    super.insertString(offset, str.toUpperCase(), attr);
                } else {
                    //nao faz nada
                    if (getLength() == iMaxLength) {
                        return;
                    }
                    String newStr = str.substring(0, (iMaxLength - getLength()));

                    super.insertString(offset, newStr.toUpperCase(), attr);
                }
            }
        }
    }
}