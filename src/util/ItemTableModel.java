package util;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;
import model.Item;

public class ItemTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;
    private List<Item> itens;

    public ItemTableModel() {
        itens = new ArrayList<Item>();
    }

    public ItemTableModel(List<Item> lista) {
        this();
        itens.addAll(lista);
    }

    @Override
    public Class<?> getColumnClass(int coluna) {
        // todas as colunas representam uma String
        return String.class;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int coluna) {
        switch (coluna) {
            case 0:
                return "Descrição Abreviada";
            case 1:
                return "Descrição";
            default:
                return "";
        }
    }

    @Override
    public int getRowCount() {
        return itens.size();
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        Item u = itens.get(linha);
        switch (coluna) {
            case 0:
                return u.getDescricao_abreviada_item();
            case 1:
                return u.getDescricao_item();
            default:
                return null;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna) {
        Item u = itens.get(linha);
        switch (coluna) {
            case 0:
                u.setDescricao_abreviada_item(valor.toString());
                break;
            case 1:
                u.setDescricao_item(valor.toString());
                break;
        }

        fireTableDataChanged();
    }

    public Item getObjeto(int index) {
        return itens.get(index);
    }

    public void adicionaLista(List<Item> lista) {
        int i = itens.size() - 1;
        itens.addAll(lista);
        fireTableRowsInserted(i, i + lista.size());
    }

    public void limpaLista() {
        int i = itens.size();
        itens.clear();
        if (i>0){
            fireTableRowsDeleted(0, i - 1);            
        }
    }
}
