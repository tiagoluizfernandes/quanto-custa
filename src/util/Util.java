package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.*;
import org.jasypt.util.text.BasicTextEncryptor;

public class Util {

    public static String criptografa(String senha) {
        BasicTextEncryptor bte = new BasicTextEncryptor();

        bte.setPassword("custo");
        return bte.encrypt(senha);
    }

    public static String descriptografa(String senha) {
        BasicTextEncryptor bte = new BasicTextEncryptor();

        bte.setPassword("custo");
        return bte.decrypt(senha);
    }

    public static boolean validaData(String data) {
        Pattern p = Pattern.compile("^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$");
        Matcher m = p.matcher(data);
        return m.find();
    }

    public static boolean validaHora(String str, String formato) {
        DateFormat df = new SimpleDateFormat(formato);
        try {
            df.setLenient(false);
            java.util.Date d = df.parse(str);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    public static float moedaToLong(String valor) {
        if (!valor.equals("")) {
            valor = valor.replaceAll("[.]", "");
            valor = valor.replaceAll(",", ".");
        } else {
            valor = "0.00";
        }
        return Float.parseFloat(valor);
    }

    public static int calculaIdade(Date data) {
        Calendar cData = Calendar.getInstance();
        Calendar cHoje = Calendar.getInstance();
        cData.setTime(data);
        cData.set(Calendar.YEAR, cHoje.get(Calendar.YEAR));
        int idade = cData.after(cHoje) ? -1 : 0;
        cData.setTime(data);
        idade += cHoje.get(Calendar.YEAR) - cData.get(Calendar.YEAR);
        return idade;
    }

    public static Connection conexao() throws Exception {
        try {
            Class.forName("org.postgresql.Driver");
            String dsn = "jdbc:postgresql://localhost:5432/postgres";
            return DriverManager.getConnection(dsn, "postgres", "admin");

        } catch (java.lang.ClassNotFoundException e) {
            System.out.println("conexao(): " + e.toString());
            return null;
        } catch (SQLException e) {
            System.out.println("conexao(): " + e.toString());
            return null;
        }
    }

    public static ArrayList<String> getListaIntervaloDatas(String dataInicio, String dataFim) throws Exception {
        ArrayList<String> lista = new ArrayList<String>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Calendar inicio = parseCal(dataInicio);
        Calendar fim = parseCal(dataFim);

        for (Calendar c = (Calendar) inicio.clone(); c.compareTo(fim) <= 0; c.add(Calendar.DATE, +1)) {
            if ((c.get(Calendar.DAY_OF_WEEK) != 1) && (c.get(Calendar.DAY_OF_WEEK) != 7)) {
                lista.add(df.format(c.getTime()));
            }
        }
        return lista;
    }

    private static Calendar parseCal(String dd_mm_yyyy) throws ParseException {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        Date dt = df.parse(dd_mm_yyyy);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        return cal;
    }
}
