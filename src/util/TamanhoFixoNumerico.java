package util;

import javax.swing.text.*;

public class TamanhoFixoNumerico extends PlainDocument {

    private int iMaxLength;

    public TamanhoFixoNumerico(int maxlen) {
        super();
        iMaxLength = maxlen;
    }

    public boolean isNumber(String n) {
        boolean is;
        try {
            Long.parseLong(n);
            is = true;
        } catch (Exception e) {
            is = false;
        }
        return is;
    }

    @Override
    public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
        if (str == null || !isNumber(str)) {
            return;
        }

        int ilen = (getLength() + str.length());
        // se o comprimento final for menor aceita a str
        if (ilen <= iMaxLength)
        {
            super.insertString(offset, str.toUpperCase(), attr);
        } else {
            //nao faz nada
            if (getLength() == iMaxLength) {
                return;
            }
            String newStr = str.substring(0, (iMaxLength - getLength()));

            super.insertString(offset, newStr.toUpperCase(), attr);
        }
    }
}
