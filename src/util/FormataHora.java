package util;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class FormataHora extends PlainDocument {

    private int iMaxLength = 5;

    @Override
    public void insertString(int offset, String str, AttributeSet attr)
            throws BadLocationException {

        if (str == null) {
            return;
        }
        if (str.length() > 1) {
            String varrer = str;
            String param = "";
            for (int i = 0; i < varrer.length(); i++) {
                str = String.valueOf(varrer.charAt(i));
                if (":".equals(str) || isNumber(str)) {
                    param = param + str;
                }
            }
            String newStr = param.substring(0, (iMaxLength - getLength()));

            super.insertString(offset, newStr.toUpperCase(), attr);
        } else {
            if (isNumber(str)) {
                if (iMaxLength <= 0) // aceitara qualquer no. de caracteres  
                {
                    super.insertString(offset, str, attr);
                    formatarHora(offset, attr);
                    return;

                }

                int ilen = (getLength() + str.length());
                if (ilen <= iMaxLength) // se o comprimento final for menor...    
                {
                    super.insertString(offset, str, attr);   // ...aceita str  
                    formatarHora(offset, attr);

                }
            }
        }
    }

    public boolean isNumber(String n) {
        boolean is;
        try {
            Long.parseLong(n);
            is = true;
        } catch (Exception e) {
            is = false;
        }
        return is;
    }

    private void formatarHora(int offset, AttributeSet attr)
            throws BadLocationException {
        if (getLength() == 3) {
            super.insertString(offset, ":", attr);
        } 
    }
}