package model;

public class Item {

    private int codigo_item;
    private String descricao_item;
    private String descricao_abreviada_item;

    public int getCodigo_item() {
        return codigo_item;
    }

    public void setCodigo_item(int codigo_item) {
        this.codigo_item = codigo_item;
    }

    public String getDescricao_item() {
        return descricao_item;
    }

    public void setDescricao_item(String descricao_item) {
        this.descricao_item = descricao_item;
    }
    
    public String getDescricao_abreviada_item() {
        return descricao_abreviada_item;
    }

    public void setDescricao_abreviada_item(String descricao_abreviada_item) {
        this.descricao_abreviada_item = descricao_abreviada_item;
    }
}
