package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;

import dao.realization.UsuarioDAO;

import model.Usuario;
import view.PrincipalVisao;
import view.EfetuarLoginVisao;

public class EfetuarLoginControle implements ActionListener, KeyListener {

    private EfetuarLoginVisao visao;

    public EfetuarLoginControle(EfetuarLoginVisao visao) {
        this.visao = visao;
        visao.configuraClick(this);
        visao.configuraKeyPress(this);
    }

    public void inicia() {
        visao.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("logar")) {
            Usuario m = visao.preencheModelo();
            if (m != null) {
                UsuarioDAO dao = new UsuarioDAO();
                try {
                    Usuario user = dao.consultarUsuario(m);
                    if (user != null) {
                        PrincipalVisao principal = new PrincipalVisao();
                        PrincipalVisao.setUsuario(m.getLogin());
                        principal.show();
                        principal.setExtendedState(principal.MAXIMIZED_BOTH);
                        principal.setVisible(true);
                        visao.dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, "Login ou senha inválidos!");
                    }
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(null, "Falha ao realizar o Login!");
                }
            }
        }
        if (e.getActionCommand().equals("sair")) {
            visao.dispose();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyChar() == '\n') {
            Usuario m = visao.preencheModelo();
            if (m != null) {
                UsuarioDAO dao = new UsuarioDAO();
                try {
                    Usuario user = dao.consultarUsuario(m);
                    if (user != null) {
                        PrincipalVisao principal = new PrincipalVisao();
                        PrincipalVisao.setUsuario(m.getLogin());
                        principal.show();
                        principal.setExtendedState(principal.MAXIMIZED_BOTH);
                        principal.setVisible(true);
                        visao.dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, "Login ou senha inválidos!");
                    }
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(null, "Falha ao realizar o Login!");
                }
            }
        }
    }
}
