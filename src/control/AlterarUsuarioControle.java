package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import dao.realization.UsuarioDAO;

import model.Usuario;
import view.AlterarUsuarioVisao;
import view.PesquisarUsuarioVisao;

public class AlterarUsuarioControle implements ActionListener {

    private AlterarUsuarioVisao visao;

    public AlterarUsuarioControle(AlterarUsuarioVisao visao) {
        this.visao = visao;
        visao.configuraClick(this);
    }

    public void inicia(Usuario usuario) {
        visao.preencheVisao(usuario);
        visao.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("alterar")) {
            Usuario m = visao.preencheModelo();
            if (m != null) {
                UsuarioDAO dao = new UsuarioDAO();
                try {
                    int ret = dao.alterarUsuario(m);
                    if (ret == 0) {
                        visao.alteracaoSucesso();
                        PesquisarUsuarioVisao tela = new PesquisarUsuarioVisao();
                        tela.show();
                        tela.setLocationRelativeTo(null);
                        visao.dispose();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Falha ao atualizar usuário!");
                }
            }
        }
        if (e.getActionCommand().equals("cancelar")) {
            if (visao.limpaTela()) {
                PesquisarUsuarioVisao tela = new PesquisarUsuarioVisao();
                tela.show();
                tela.setLocationRelativeTo(null);
                visao.dispose();
            }
        }
        if (e.getActionCommand().equals("sair")) {
            visao.dispose();
        }
    }
}
