package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import dao.realization.ItemDAO;

import model.Item;
import view.AlterarItemVisao;
import view.PesquisarItemVisao;

public class AlterarItemControle implements ActionListener {

    private AlterarItemVisao visao;

    public AlterarItemControle(AlterarItemVisao visao) {
        this.visao = visao;
        visao.configuraClick(this);
    }

    public void inicia(Item item) {
        visao.preencheVisao(item);
        visao.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("alterar")) {
            Item m = visao.preencheModelo();
            if (m != null) {
                ItemDAO dao = new ItemDAO();
                try {
                    int ret = dao.alterarItem(m);
                    if (ret == 0) {
                        visao.alteracaoSucesso();
                        PesquisarItemVisao tela = new PesquisarItemVisao();
                        tela.show();
                        tela.setLocationRelativeTo(null);
                        visao.dispose();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Falha ao atualizar item!");
                }
            }
        }
        if (e.getActionCommand().equals("cancelar")) {
            if (visao.limpaTela()) {
                PesquisarItemVisao tela = new PesquisarItemVisao();
                tela.show();
                tela.setLocationRelativeTo(null);
                visao.dispose();
            }
        }
        if (e.getActionCommand().equals("sair")) {
            visao.dispose();
        }
    }
}
