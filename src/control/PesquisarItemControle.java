package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import dao.realization.ItemDAO;
import java.util.List;

import model.Item;
import view.AlterarItemVisao;
import view.PesquisarItemVisao;

public class PesquisarItemControle implements ActionListener {

    private PesquisarItemVisao visao;

    public PesquisarItemControle(PesquisarItemVisao visao) {
        this.visao = visao;
        visao.configuraClick(this);
    }

    public void inicia() {
        visao.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("pesquisar")) {
            Item m = visao.preencheModelo();
            if (m != null) {
                ItemDAO dao = new ItemDAO();
                try {
                    List<Item> produto = dao.consultarItem(m);
                    if (!produto.isEmpty()) {
                        visao.preencheVisao(produto);
                    } else {
                        JOptionPane.showMessageDialog(null, "Nenhum item encontrado!");
                        visao.limpaVisao();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Falha ao pesquisar item!");
                }
            }
        }
        if (e.getActionCommand().equals("editar")) {
            AlterarItemVisao tela = new AlterarItemVisao(visao.selecionaModelo());
            tela.show();
            tela.setLocationRelativeTo(null);
            visao.dispose();
        }
        if (e.getActionCommand().equals("sair")) {
            visao.dispose();
        }
    }
}
