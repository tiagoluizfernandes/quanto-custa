package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import dao.realization.ItemDAO;

import model.Item;
import view.CadastrarItemVisao;

public class CadastrarItemControle implements ActionListener {

    private CadastrarItemVisao visao;

    public CadastrarItemControle(CadastrarItemVisao visao) {
        this.visao = visao;
        visao.configuraClick(this);
    }

    public void inicia() {
        visao.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("salvar")) {
            Item m = visao.preencheModelo();
            if (m != null) {
                ItemDAO dao = new ItemDAO();
                try {
                    int ret = dao.inserirItem(m);
                    if (ret == 0) {
                        visao.cadastroSucesso();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Falha ao inserir item!");
                }
            }
        }
        if (e.getActionCommand().equals("cancelar")) {
            visao.limpaTela();
        }
        if (e.getActionCommand().equals("sair")) {
            visao.dispose();
        }
    }
}
