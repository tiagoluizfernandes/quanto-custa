package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import dao.realization.FornecedorDAO;

import model.Fornecedor;
import view.AlterarFornecedorVisao;
import view.PesquisarFornecedorVisao;

public class AlterarFornecedorControle implements ActionListener {

    private AlterarFornecedorVisao visao;

    public AlterarFornecedorControle(AlterarFornecedorVisao visao) {
        this.visao = visao;
        visao.configuraClick(this);
    }

    public void inicia(Fornecedor fornecedor) {
        visao.preencheVisao(fornecedor);
        visao.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("alterar")) {
            Fornecedor m = visao.preencheModelo();
            if (m != null) {
                FornecedorDAO dao = new FornecedorDAO();
                try {
                    int ret = dao.alterarFornecedor(m);
                    if (ret == 0) {
                        visao.alteracaoSucesso();
                        PesquisarFornecedorVisao tela = new PesquisarFornecedorVisao();
                        tela.show();
                        tela.setLocationRelativeTo(null);
                        visao.dispose();
                    } else if(ret == 1) {
                        JOptionPane.showMessageDialog(null, "Já existe um Fornecedor cadastrado com este CNPJ!");
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Falha ao atualizar fornecedor!");
                }
            }
        }
        if (e.getActionCommand().equals("cancelar")) {
            if (visao.limpaTela()) {
                PesquisarFornecedorVisao tela = new PesquisarFornecedorVisao();
                tela.show();
                tela.setLocationRelativeTo(null);
                visao.dispose();
            }
        }
        if (e.getActionCommand().equals("sair")) {
            visao.dispose();
        }
    }
}
