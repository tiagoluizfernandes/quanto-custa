package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import view.CadastrarFornecedorVisao;
import view.CadastrarItemVisao;
import view.CadastrarUsuarioVisao;
import view.PrincipalVisao;
import view.PesquisarFornecedorVisao;
import view.PesquisarItemVisao;
import view.PesquisarUsuarioVisao;

public class PrincipalControle implements ActionListener {

    private PrincipalVisao visao;

    public PrincipalControle(PrincipalVisao visao) {
        this.visao = visao;
        visao.configuraClick(this);
    }

    public void inicia() {
        visao.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("cadastrarUsuario")) {
            CadastrarUsuarioVisao tela = new CadastrarUsuarioVisao();
            tela.show();
            tela.setLocationRelativeTo(null);
        }
        if (e.getActionCommand().equals("pesquisarUsuario")) {
            PesquisarUsuarioVisao tela = new PesquisarUsuarioVisao();
            tela.show();
            tela.setLocationRelativeTo(null);
        }
        if (e.getActionCommand().equals("cadastrarItem")) {
            CadastrarItemVisao tela = new CadastrarItemVisao();
            tela.show();
            tela.setLocationRelativeTo(null);
        }
        if (e.getActionCommand().equals("pesquisarItem")) {
            PesquisarItemVisao tela = new PesquisarItemVisao();
            tela.show();
            tela.setLocationRelativeTo(null);
        }
        if (e.getActionCommand().equals("cadastrarFornecedor")) {
            CadastrarFornecedorVisao tela = new CadastrarFornecedorVisao();
            tela.show();
            tela.setLocationRelativeTo(null);
        }
        if (e.getActionCommand().equals("pesquisarFornecedor")) {
            PesquisarFornecedorVisao tela = new PesquisarFornecedorVisao();
            tela.show();
            tela.setLocationRelativeTo(null);
        }
    }
}
