package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import dao.realization.FornecedorDAO;

import model.Fornecedor;
import view.CadastrarFornecedorVisao;

public class CadastrarFornecedorControle implements ActionListener {

    private CadastrarFornecedorVisao visao;

    public CadastrarFornecedorControle(CadastrarFornecedorVisao visao) {
        this.visao = visao;
        visao.configuraClick(this);
    }

    public void inicia() {
        visao.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("salvar")) {
            Fornecedor m = visao.preencheModelo();
            if (m != null) {
                FornecedorDAO dao = new FornecedorDAO();
                try {
                    int ret = dao.inserirFornecedor(m);
                    if (ret == 0) {
                        visao.cadastroSucesso();
                    } else if(ret == 1) {
                        JOptionPane.showMessageDialog(null, "Já existe um Fornecedor cadastrado com este CNPJ!");
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Falha ao inserir fornecedor!");
                }
            }
        }
        if (e.getActionCommand().equals("cancelar")) {
            visao.limpaTela();
        }
        if (e.getActionCommand().equals("sair")) {
            visao.dispose();
        }
    }
}
