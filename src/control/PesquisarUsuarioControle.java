package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import dao.realization.UsuarioDAO;
import java.util.List;

import model.Usuario;
import view.AlterarUsuarioVisao;
import view.PesquisarUsuarioVisao;

public class PesquisarUsuarioControle implements ActionListener {

    private PesquisarUsuarioVisao visao;

    public PesquisarUsuarioControle(PesquisarUsuarioVisao visao) {
        this.visao = visao;
        visao.configuraClick(this);
    }

    public void inicia() {
        visao.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("pesquisar")) {
            Usuario m = visao.preencheModelo();
            if (m != null) {
                UsuarioDAO dao = new UsuarioDAO();
                try {
                    List<Usuario> user = dao.pesquisarUsuario(m);
                    if (!user.isEmpty()) {
                        visao.preencheVisao(user);
                    } else {
                        JOptionPane.showMessageDialog(null, "Nenhum usuário encontrado!");
                        visao.limpaVisao();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Falha ao pesquisar usuário!");
                }
            }
        }
        if (e.getActionCommand().equals("editar")) {
            AlterarUsuarioVisao tela = new AlterarUsuarioVisao(visao.selecionaModelo());
            tela.show();
            tela.setLocationRelativeTo(null);
            visao.dispose();
        }
        if (e.getActionCommand().equals("sair")) {
            visao.dispose();
        }
    }
}
