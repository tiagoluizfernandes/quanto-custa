package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import dao.realization.UsuarioDAO;

import model.Usuario;
import view.CadastrarUsuarioVisao;

public class CadastrarUsuarioControle implements ActionListener {

    private CadastrarUsuarioVisao visao;

    public CadastrarUsuarioControle(CadastrarUsuarioVisao visao) {
        this.visao = visao;
        visao.configuraClick(this);
    }

    public void inicia() {
        visao.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("salvar")) {
            Usuario m = visao.preencheModelo();
            if (m != null) {
                UsuarioDAO dao = new UsuarioDAO();
                try {
                    int ret = dao.inserirUsuario(m);
                    if (ret == 0) {
                        visao.cadastroSucesso();
                    } else if(ret == 1) {
                        JOptionPane.showMessageDialog(null, "Este login já está cadastrado!");
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Falha ao inserir usuário!");
                }
            }
        }
        if (e.getActionCommand().equals("cancelar")) {
            visao.limpaTela();
        }
        if (e.getActionCommand().equals("sair")) {
            visao.dispose();
        }
    }
}
