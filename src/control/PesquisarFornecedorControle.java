package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import dao.realization.FornecedorDAO;
import java.util.List;

import model.Fornecedor;
import view.AlterarFornecedorVisao;
import view.PesquisarFornecedorVisao;

public class PesquisarFornecedorControle implements ActionListener {

    private PesquisarFornecedorVisao visao;

    public PesquisarFornecedorControle(PesquisarFornecedorVisao visao) {
        this.visao = visao;
        visao.configuraClick(this);
    }

    public void inicia() {
        visao.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("pesquisar")) {
            Fornecedor m = visao.preencheModelo();
            if (m != null) {
                FornecedorDAO dao = new FornecedorDAO();
                try {
                    List<Fornecedor> fornecedor = dao.consultarFornecedor(m, visao.getAmbos());
                    if (!fornecedor.isEmpty()) {
                        visao.preencheVisao(fornecedor);
                    } else {
                        JOptionPane.showMessageDialog(null, "Nenhum fornecedor encontrado!");
                        visao.limpaVisao();
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Falha ao pesquisar fornecedor!");
                }
            }
        }
        if (e.getActionCommand().equals("editar")) {
            AlterarFornecedorVisao tela = new AlterarFornecedorVisao(visao.selecionaModelo());
            tela.show();
            tela.setLocationRelativeTo(null);
            visao.dispose();
        }
        if (e.getActionCommand().equals("sair")) {
            visao.dispose();
        }
    }
}
