package view;

import control.AlterarFornecedorControle;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import model.Fornecedor;
import util.FormataCEP;
import util.FormataCNPJ;
import util.TamanhoFixo;
import util.TamanhoFixoComEspaco;
import util.TamanhoFixoNumerico;
import util.ValidaCNPJ;

public class AlterarFornecedorVisao extends javax.swing.JFrame {

    public AlterarFornecedorVisao(Fornecedor fornecedor) {
        initComponents();
        jTFRazao.setDocument(new TamanhoFixoComEspaco(100));
        jTFNome.setDocument(new TamanhoFixoComEspaco(100));
        jTFCNPJ.setDocument(new FormataCNPJ());
        jTFRamo.setDocument(new TamanhoFixoComEspaco(100));
        jTFEndereco.setDocument(new TamanhoFixoComEspaco(100));
        jTFNumero.setDocument(new TamanhoFixoNumerico(6));
        jTFComplemento.setDocument(new TamanhoFixo(10));
        jTFCEP.setDocument(new FormataCEP());
        jTFCidade.setDocument(new TamanhoFixoComEspaco(30));
        jTFDigTelefone.setDocument(new TamanhoFixoNumerico(2));
        jTFTelefone.setDocument(new TamanhoFixoNumerico(9));
        jTFContato.setDocument(new TamanhoFixoComEspaco(100));
        jTFCodigo.setVisible(false);
        AlterarFornecedorControle controle = new AlterarFornecedorControle(this);

        controle.inicia(fornecedor);
    }

    public Fornecedor preencheModelo() {
        int codigo_fornecedor = Integer.parseInt(jTFCodigo.getText());
        String razao = jTFRazao.getText();
        String nome = jTFNome.getText();
        String cnpj = jTFCNPJ.getText();
        String ramo = jTFRamo.getText();
        String endereco = jTFEndereco.getText();
        String numero = jTFNumero.getText();
        String complemento = jTFComplemento.getText();
        String cep = jTFCEP.getText();
        String cidade = jTFCidade.getText();
        String estado = jCBEstado.getSelectedItem().toString().replace(" ", "");
        String contato = jTFContato.getText();
        boolean ativo = jCBAtivo.isSelected();

        if (razao.equals("")) {
            JOptionPane.showMessageDialog(null, "O campo \"Razão Social\" deve ser preenchido!");
            jTFRazao.requestFocusInWindow();
            return null;
        }
        if (nome.equals("")) {
            JOptionPane.showMessageDialog(null, "O campo \"Nome Fantasia\" deve ser preenchido!");
            jTFNome.requestFocusInWindow();
            return null;
        }
        if (cnpj.equals("")) {
            JOptionPane.showMessageDialog(null, "O campo \"CNPJ\" deve ser preenchido!");
            jTFCNPJ.requestFocusInWindow();
            return null;
        } else {
            ValidaCNPJ validaCNPJ = new ValidaCNPJ();
            String cnpj1 = cnpj.replaceAll("[^0-9]", "");
            if (!validaCNPJ.isCNPJ(cnpj1)) {
                JOptionPane.showMessageDialog(null, "O CNPJ é inválido!");
                jTFCNPJ.requestFocusInWindow();
                return null;
            }
        }
        if (endereco.equals("")) {
            JOptionPane.showMessageDialog(null, "O campo \"Endereço\" deve ser preenchido!");
            jTFEndereco.requestFocusInWindow();
            return null;
        }
        if (numero.equals("")) {
            JOptionPane.showMessageDialog(null, "O campo \"Número\" deve ser preenchido!");
            jTFNumero.requestFocusInWindow();
            return null;
        }
        if (cep.equals("")) {
            JOptionPane.showMessageDialog(null, "O campo \"CEP\" deve ser preenchido!");
            jTFCEP.requestFocusInWindow();
            return null;
        } else if (cep.length() != 10) {
            JOptionPane.showMessageDialog(null, "O CEP é inválido!");
            jTFCEP.requestFocusInWindow();
            return null;
        }
        if (cidade.equals("")) {
            JOptionPane.showMessageDialog(null, "O campo \"Cidade\" deve ser preenchido!");
            jTFCidade.requestFocusInWindow();
            return null;
        }
        if (estado.equals("")) {
            JOptionPane.showMessageDialog(null, "O campo \"Estado\" deve ser preenchido!");
            jCBEstado.requestFocusInWindow();
            return null;
        }
        String telefone = "";
        if (jTFDigTelefone.getText().equals("") && jTFTelefone.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Digite o Código de área e Telefone!");
            jTFDigTelefone.requestFocusInWindow();
            return null;
        } else {
            if (!jTFDigTelefone.getText().equals("") && !jTFTelefone.getText().equals("")) {
                if ((jTFDigTelefone.getText().length() != 2) || (jTFTelefone.getText().length() < 8)) {
                    JOptionPane.showMessageDialog(null, "Código de área ou Telefone inválido(s)!");
                    jTFDigTelefone.requestFocusInWindow();
                    return null;
                } else {
                    telefone = jTFDigTelefone.getText() + jTFTelefone.getText();
                }
            } else {
                JOptionPane.showMessageDialog(null, "Digite o Código de área e Telefone!");
                jTFDigTelefone.requestFocusInWindow();
                return null;
            }
        }

        Fornecedor m = new Fornecedor();
        m.setCodigo_fornecedor(codigo_fornecedor);
        m.setRazao(razao);
        m.setNome(nome);
        m.setCnpj(cnpj);
        m.setRamo(ramo);
        m.setEndereco(endereco);
        m.setNumero(numero);
        m.setComplemento(complemento);
        m.setCidade(cidade);
        m.setCep(cep);
        m.setEstado(estado);
        m.setTelefone(telefone);
        m.setContato(contato);
        m.setAtivo(ativo);
        return m;
    }

    public void preencheVisao(Fornecedor fornecedor) {
        jTFCodigo.setText(String.valueOf(fornecedor.getCodigo_fornecedor()));
        jTFRazao.setText(fornecedor.getRazao());
        jTFNome.setText(fornecedor.getNome());
        jTFCNPJ.setText(fornecedor.getCnpj());
        jTFRamo.setText(fornecedor.getRamo());
        jTFEndereco.setText(fornecedor.getEndereco());
        jTFNumero.setText(String.valueOf(fornecedor.getNumero()));
        jTFComplemento.setText(fornecedor.getComplemento());
        jTFCEP.setText(fornecedor.getCep());
        jTFCidade.setText(fornecedor.getCidade());
        jCBEstado.setSelectedItem(fornecedor.getEstado());
        if (fornecedor.getTelefone() != null && !fornecedor.getTelefone().equals("")) {
            jTFDigTelefone.setText(fornecedor.getTelefone().substring(0, 2));
            jTFTelefone.setText(fornecedor.getTelefone().substring(2, fornecedor.getTelefone().length()));
        }
        jTFContato.setText(fornecedor.getContato());
        jCBAtivo.setSelected(fornecedor.isAtivo());
    }

    public boolean limpaTela() {
        if (JOptionPane.showConfirmDialog(null, "Tem certeza que deseja cancelar a alteração?", "Input", JOptionPane.YES_NO_OPTION) == 0) {
            return true;
        }
        return false;
    }

    public void alteracaoSucesso() {
        JOptionPane.showMessageDialog(null, "Alteração realizada com sucesso!");
    }

    public void configuraClick(ActionListener ouvinte) {
        btnAlterar.setActionCommand("alterar");
        btnAlterar.addActionListener(ouvinte);

        btnCancelar.setActionCommand("cancelar");
        btnCancelar.addActionListener(ouvinte);

        btnSair.setActionCommand("sair");
        btnSair.addActionListener(ouvinte);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoRadioSexo = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jTFRazao = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jTFCNPJ = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jTFContato = new javax.swing.JTextField();
        jCBAtivo = new javax.swing.JCheckBox();
        jLabel7 = new javax.swing.JLabel();
        jTFNumero = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jTFEndereco = new javax.swing.JTextField();
        jTFCEP = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTFComplemento = new javax.swing.JTextField();
        jTFCidade = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jCBEstado = new javax.swing.JComboBox();
        jLabel24 = new javax.swing.JLabel();
        jTFDigTelefone = new javax.swing.JTextField();
        jTFTelefone = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jTFRamo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTFNome = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTFCodigo = new javax.swing.JTextField();
        panelBotoes = new javax.swing.JPanel();
        btnCancelar = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Alterar Fornecedor");

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("*Razão Social:");

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel16.setText("Contato:");

        jCBAtivo.setText("Ativo");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("*Endereço:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("*Numero:");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("*CEP:");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("*Cidade:");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Complemento:");

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("*Estado:");

        jCBEstado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " ", "AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RR", "RS", "SC", "SE", "SP", "TO" }));
        jCBEstado.setToolTipText("");

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel24.setText("*Telefone:");

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel19.setText("Ramo:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("*CNPJ:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("*Nome Fantasia:");

        jTFCodigo.setEditable(false);
        jTFCodigo.setEnabled(false);
        jTFCodigo.setRequestFocusEnabled(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel19, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel24, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jTFDigTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTFTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jTFComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel10)
                                .addGap(6, 6, 6)
                                .addComponent(jTFCEP, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jTFCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCBEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jCBAtivo)
                                .addGap(82, 82, 82)
                                .addComponent(jTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jTFRazao, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTFRamo, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jTFContato, javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                    .addComponent(jTFEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jLabel8)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jTFNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jTFNome, javax.swing.GroupLayout.Alignment.LEADING))
                            .addComponent(jTFCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTFRazao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTFNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTFCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTFRamo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTFEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTFNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, 18, Short.MAX_VALUE)
                    .addComponent(jTFComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTFCEP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTFCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(jCBEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTFDigTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTFTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jTFContato, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCBAtivo, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTFCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(61, 61, 61))
        );

        panelBotoes.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnCancelar.setText("Cancelar");

        btnSair.setText("Sair");
        btnSair.setToolTipText("");

        btnAlterar.setText("Salvar");
        btnAlterar.setToolTipText("");

        javax.swing.GroupLayout panelBotoesLayout = new javax.swing.GroupLayout(panelBotoes);
        panelBotoes.setLayout(panelBotoesLayout);
        panelBotoesLayout.setHorizontalGroup(
            panelBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBotoesLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAlterar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSair)
                .addContainerGap())
        );
        panelBotoesLayout.setVerticalGroup(
            panelBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBotoesLayout.createSequentialGroup()
                .addGap(0, 11, Short.MAX_VALUE)
                .addGroup(panelBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancelar)
                    .addComponent(btnSair)
                    .addComponent(btnAlterar))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(panelBotoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnSair;
    private javax.swing.ButtonGroup grupoRadioSexo;
    private javax.swing.JCheckBox jCBAtivo;
    private javax.swing.JComboBox jCBEstado;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField jTFCEP;
    private javax.swing.JTextField jTFCNPJ;
    private javax.swing.JTextField jTFCidade;
    private javax.swing.JTextField jTFCodigo;
    private javax.swing.JTextField jTFComplemento;
    private javax.swing.JTextField jTFContato;
    private javax.swing.JTextField jTFDigTelefone;
    private javax.swing.JTextField jTFEndereco;
    private javax.swing.JTextField jTFNome;
    private javax.swing.JTextField jTFNumero;
    private javax.swing.JTextField jTFRamo;
    private javax.swing.JTextField jTFRazao;
    private javax.swing.JTextField jTFTelefone;
    private javax.swing.JPanel panelBotoes;
    // End of variables declaration//GEN-END:variables
}
