package view;

import control.PrincipalControle;
import java.awt.event.ActionListener;

public class PrincipalVisao extends javax.swing.JFrame {

    private static String usuario;

    public static void setUsuario(String usuario) {
        PrincipalVisao.usuario = usuario;
    }

    public static String getUsuario() {
        return usuario;
    }
    
    public void configuraClick(ActionListener ouvinte) {
        
        //Menu Usuario
        jMenuItem1.setActionCommand("cadastrarUsuario");
        jMenuItem1.addActionListener(ouvinte);
        jMenuItem2.setActionCommand("pesquisarUsuario");
        jMenuItem2.addActionListener(ouvinte);
        
        //Menu Criança
        /*jMenuItem3.setActionCommand("cadastrarCrianca");
        jMenuItem3.addActionListener(ouvinte);
        jMenuItem4.setActionCommand("pesquisarCrianca");
        jMenuItem4.addActionListener(ouvinte);
        jMenuItem17.setActionCommand("gerarRelCrianca");
        jMenuItem17.addActionListener(ouvinte);
        
        //Menu Funcionário
        jMenuItem5.setActionCommand("cadastrarFuncionario");
        jMenuItem5.addActionListener(ouvinte);
        jMenuItem6.setActionCommand("pesquisarFuncionario");
        jMenuItem6.addActionListener(ouvinte);
        jMenuItem18.setActionCommand("gerarRelFuncionario");
        jMenuItem18.addActionListener(ouvinte);
        
        //Menu Ponto
        jMenuItem30.setActionCommand("cadastrarPonto");
        jMenuItem30.addActionListener(ouvinte);
        jMenuItem22.setActionCommand("pesquisarPonto");
        jMenuItem22.addActionListener(ouvinte);
        jMenuItem31.setActionCommand("gerarRelPonto");
        jMenuItem31.addActionListener(ouvinte);
        
        //Menu Parceiro
        jMenuItem7.setActionCommand("cadastrarParceiro");
        jMenuItem7.addActionListener(ouvinte);
        jMenuItem8.setActionCommand("pesquisarParceiro");
        jMenuItem8.addActionListener(ouvinte);
        jMenuItem19.setActionCommand("gerarRelParceiro");
        jMenuItem19.addActionListener(ouvinte);
        jMenuItem26.setActionCommand("gerarRelApadrinhamento");
        jMenuItem26.addActionListener(ouvinte);
        
        //Menu Receita
        jMenuItem9.setActionCommand("cadastrarReceita");
        jMenuItem9.addActionListener(ouvinte);
        jMenuItem10.setActionCommand("pesquisarReceita");
        jMenuItem10.addActionListener(ouvinte);
        jMenuItem20.setActionCommand("gerarRelReceita");
        jMenuItem20.addActionListener(ouvinte);*/
        
        //Menu Item
        jMenuItem24.setActionCommand("cadastrarItem");
        jMenuItem24.addActionListener(ouvinte);
        jMenuItem25.setActionCommand("pesquisarItem");
        jMenuItem25.addActionListener(ouvinte);
        
        //Menu Estoque
        /*jMenuItem11.setActionCommand("cadastrarEstoque");
        jMenuItem11.addActionListener(ouvinte);
        jMenuItem12.setActionCommand("pesquisarEstoque");
        jMenuItem12.addActionListener(ouvinte);
        jMenuItem21.setActionCommand("gerarRelEstoque");
        jMenuItem21.addActionListener(ouvinte);*/
        
        //Menu Fornecedor
        jMenuItem27.setActionCommand("cadastrarFornecedor");
        jMenuItem27.addActionListener(ouvinte);
        jMenuItem28.setActionCommand("pesquisarFornecedor");
        jMenuItem28.addActionListener(ouvinte);
        
        //Menu Documento
        /*jMenuItem13.setActionCommand("cadastrarDocumento");
        jMenuItem13.addActionListener(ouvinte);
        jMenuItem14.setActionCommand("pesquisarDocumento");
        jMenuItem14.addActionListener(ouvinte);
        
        //Menu Campanha
        jMenuItem15.setActionCommand("cadastrarCampanha");
        jMenuItem15.addActionListener(ouvinte);
        jMenuItem16.setActionCommand("pesquisarCampanha");
        jMenuItem16.addActionListener(ouvinte);
        jMenuItem23.setActionCommand("gerarRelCampanha");
        jMenuItem23.addActionListener(ouvinte);*/

    }

    public PrincipalVisao() {
        initComponents();
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        setExtendedState(MAXIMIZED_BOTH);
        PrincipalControle controle = new PrincipalControle(this);
        controle.inicia();
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu10 = new javax.swing.JMenu();
        jMenuItem24 = new javax.swing.JMenuItem();
        jMenuItem25 = new javax.swing.JMenuItem();
        jMenu11 = new javax.swing.JMenu();
        jMenuItem27 = new javax.swing.JMenuItem();
        jMenuItem28 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("CUSTO - TELA PRINCIPAL");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setExtendedState(MAXIMIZED_BOTH);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/imagem.jpg"))); // NOI18N
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setFocusable(false);
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel1.setRequestFocusEnabled(false);
        jLabel1.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);

        jMenuBar1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jMenu1.setBorder(null);
        jMenu1.setText("Usuário");

        jMenuItem1.setText("Cadastrar");
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Pesquisar/Alterar");
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu10.setText("Item");

        jMenuItem24.setText("Cadastrar");
        jMenu10.add(jMenuItem24);

        jMenuItem25.setText("Pesquisar/Alterar");
        jMenu10.add(jMenuItem25);

        jMenuBar1.add(jMenu10);

        jMenu11.setText("Fornecedor");

        jMenuItem27.setText("Cadastrar");
        jMenu11.add(jMenuItem27);

        jMenuItem28.setText("Pesquisar/Alterar");
        jMenu11.add(jMenuItem28);

        jMenuBar1.add(jMenu11);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(300, 300, 300)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(399, 399, 399))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(172, 172, 172)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(177, 177, 177))
        );

        setSize(new java.awt.Dimension(915, 542));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu10;
    private javax.swing.JMenu jMenu11;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem24;
    private javax.swing.JMenuItem jMenuItem25;
    private javax.swing.JMenuItem jMenuItem27;
    private javax.swing.JMenuItem jMenuItem28;
    // End of variables declaration//GEN-END:variables
}
